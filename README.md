The content of the src directory of this project is intended to be 
copied to the 'share' directory of your machine - i.e. /usr/share, 
/usr/local/share or /usr/pkg/share, depending on your operating system.

See src/vegetal-dreams/README for more information.

Latest version is available at https://gitlab.com/20-100-2fe/vegetal-dreams
